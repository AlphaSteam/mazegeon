from distutils.core import setup
from Cython.Build import cythonize
from distutils.extension import Extension
from Cython.Compiler import Options
import numpy

#Options.embed = "main"
extensions = [

 Extension("*", ["*.pyx"],
        include_dirs=[numpy.get_include()]

           )

]


setup(

    ext_modules=cythonize(extensions)
)
