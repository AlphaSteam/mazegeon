# cython: language_level=3

import sys

import CreateMaze as cm
import OpenGL.GL as gl
import basic_shapes as bs
import easy_shaders as es
import freetype
import glfw
import lighting_shaders as ls
import numpy as np
import scene_graph as sg
import transformations as tr
from PIL import Image
import Plane as pl
import pygame as pg
from screeninfo import get_monitors


INT_BYTES = 4
WIDTH = get_monitors()[0].width
HEIGHT = get_monitors()[0].height

class Character():

    def __init__(self, id, size, bearing, advance):
        self.textureID = id
        self.size = size
        self.bearing = bearing
        self.advance = advance


# A class to store the application control
class Controller:

    def __init__(self):
        self.fillPolygon = True
        # Positions
        self.x = 0
        self.y = 0
        self.z = 0
        self.origin = np.array([self.x, self.y, self.z])
        self.x_start = 0
        self.y_start = 0
        self.x_exit = 0
        self.y_exit = 0
        # Map requirements
        self.start = False
        self.exit = False

        self.camera_theta_fixed = np.pi / 2
        self.camera_rho_fixed = np.pi / 2
        self.fov = 90
        self.fullScreen = True

        # Maze
        # Clearable
        self.walls = np.array([])
        self.grounds = np.array([])
        self.ceilings = np.array([])
        self.activeLevel = 0
        # Permanent
        self.levels = {}
        self.texturePalletes = {}
        self.level = 1

        self.noclip = False

        # Lighting
        self.quadratic = 0.1
        self.lineal = 0.1
        self.constant = 0.1
        self.ka = np.array([0, 0, 0])
        self.kd = np.array([0, 0, 0])
        self.ks = np.array([0, 0, 0])
        self.shininess = 10
        self.night = True

        self.distance = 12
        # 0:North
        # 1:East
        # 2:South
        # 3:West
        self.lookDirection = 0

        self.sumaX = 0
        self.sumaY = 0

        self.inventory = False
        self.pause = False

        self.compress = False

        self.characters = {}

        self.width = WIDTH
        self.height = HEIGHT

        self.fps = 0
        self.showFps = False

        # RayCast
        self.rayCast = np.array([])
        self.ray_eye = np.array([])
        self.ray_clip = np.array([])
        self.select = False

        self.normalZ = np.array([0, 0, 0])
        self.normalY = np.array([0, 0, 0])
        self.normalX = np.array([0, 0, 0])

        self.selected = 0
        self.ended = False
        self.debug = False

        self.volume = 0.2
        self.arrows = np.array([])
        self.compass = False
        self.rotation = 0


class TexturePalette:
    def __init__(self, wall, wallCompressed, ceiling, ceilingCompressed, floor, floorCompressed, quadratic, lineal,
                 constant, ka, kd, ks, shininess):
        self.wall = wall
        self.wallCompressed = wallCompressed
        self.ceiling = ceiling
        self.ceilingCompressed = ceilingCompressed
        self.floor = floor
        self.floorCompressed = floorCompressed

        self.quadratic = quadratic
        self.lineal = lineal
        self.constant = constant
        self.ka = ka
        self.kd = kd
        self.ks = ks
        self.shininess = shininess


class Level:
    def __init__(self, maze, palette, song):
        self.maze = maze
        self.palette = palette
        self.song = song


class FrustumG:
    def __init__(self):
        self.ntl = np.zeros(3)
        self.ntr = np.zeros(3)
        self.nbl = np.zeros(3)
        self.nbr = np.zeros(3)
        self.ftl = np.zeros(3)
        self.ftr = np.zeros(3)
        self.fbl = np.zeros(3)
        self.fbr = np.zeros(3)
        self.nearD = 0.0
        self.farD = 0.0
        self.ratio = 0.0
        self.angle = 0.0
        self.tang = 0.0
        self.nw = 0.0
        self.nh = 0.0
        self.fw = 0.0
        self.fh = 0.0
        self.plane = np.empty(6, dtype=pl.Plane)
        for i in range(6):
            self.plane[i] = pl.Plane()

    def setCamInternals(self, angle, ratio, nearD, farD):
        self.ratio = ratio
        self.angle = angle
        self.nearD = nearD
        self.farD = farD

        ANG2RAD = 3.14159265358979323846 / 180.0
        tang = float(np.tan(ANG2RAD * angle * 0.5))
        self.tang = tang
        nh = nearD * tang
        self.nh = nh
        nw = nh * ratio
        self.nw = nw
        fh = farD * tang
        self.fh = fh
        fw = fh * ratio
        self.fw = fw

    def sphereInFrustum(self, p, radius):
        # 0=Outside
        # 1=Intersect
        # 2=Inside
        result = 2
        for i in range(6):
            distance = self.plane[i].distance(p)
            if distance < -radius:
                return 0
            elif distance < radius:
                result = 1
        return result

    def setCamDef(self, p, l, u):
        # 0 = Top
        # 1 = Bottom
        # 2 = Left
        # 3 = Right
        # 4 = Nearp
        # 5 = Farp

        Y = np.subtract(p, l)
        controller.normalY = Y
        if (np.sqrt(Y[0] ** 2 + Y[1] ** 2 + Y[2] ** 2)) != 0:
            Y = Y / (np.sqrt(Y[0] ** 2 + Y[1] ** 2 + Y[2] ** 2))

        X = np.multiply(u, Y)
        controller.normalX = X
        if (np.sqrt(X[0] ** 2 + X[1] ** 2 + X[2] ** 2)) != 0:
            X = X / (np.sqrt(X[0] ** 2 + X[1] ** 2 + X[2] ** 2))

        Z = np.multiply(Y, X)
        controller.normalZ = Z

        nc = np.subtract(p, np.multiply(Y, self.nearD))
        self.nc = nc
        fc = np.subtract(p, np.multiply(Y, self.farD))
        self.fc = fc
        self.plane[4].setNormalAndPoint(-Y, nc)
        self.plane[5].setNormalAndPoint(Y, fc)

        aux = np.subtract(np.add(nc, np.multiply(Z, self.nh)), p)
        if (np.sqrt(aux[0] ** 2 + aux[1] ** 2 + aux[2] ** 2)) != 0:
            aux = aux / (np.sqrt(aux[0] ** 2 + aux[1] ** 2 + aux[2] ** 2))

        normal = np.multiply(aux, X)
        self.plane[0].setNormalAndPoint(normal, np.add(nc, np.multiply(Z, self.nh)))

        aux = np.subtract(np.subtract(nc, np.multiply(Z, self.nh)), p)
        if (np.sqrt(aux[0] ** 2 + aux[1] ** 2 + aux[2] ** 2)) != 0:
            aux = aux / (np.sqrt(aux[0] ** 2 + aux[1] ** 2 + aux[2] ** 2))
        normal = np.multiply(aux, X)
        self.plane[1].setNormalAndPoint(normal, np.subtract(nc, np.multiply(Z, self.nh)))

        aux = np.subtract(np.subtract(nc, np.multiply(X, self.nh)), p)
        if (np.sqrt(aux[0] ** 2 + aux[1] ** 2 + aux[2] ** 2)) != 0:
            aux = aux / (np.sqrt(aux[0] ** 2 + aux[1] ** 2 + aux[2] ** 2))
        normal = np.multiply(aux, Z)
        self.plane[2].setNormalAndPoint(normal, np.subtract(nc, np.multiply(X, self.nh)))

        aux = np.subtract(np.add(nc, np.multiply(X, self.nh)), p)
        if (np.sqrt(aux[0] ** 2 + aux[1] ** 2 + aux[2] ** 2)) != 0:
            aux = aux / (np.sqrt(aux[0] ** 2 + aux[1] ** 2 + aux[2] ** 2))
        normal = np.multiply(aux, Z)
        self.plane[3].setNormalAndPoint(normal, np.add(nc, np.multiply(X, self.nh)))


# We will use the global controller as communication with the callback function
controller = Controller()  # Here we declare this as a global variable.
frustum = FrustumG()
t = 0


def checkExit():
    if not controller.ended:
        global t
        if controller.x == controller.x_exit and controller.y == controller.y_exit:
            controller.level += 1
            if controller.level in controller.levels:
                changeLevel(controller.levels[controller.level])
            else:
                controller.ended = True
                tf = glfw.get_time()
                t = tf - tin

            # sys.exit()


def on_key(window, key, scancode, action, mods):
    checkExit()
    global controller  # Declares that we are going to use the global object controller inside this function.
    if controller.ended:
        if action == glfw.PRESS and key == glfw.KEY_ENTER:
            reset()

    if action == glfw.PRESS and key == glfw.KEY_P:
        pass
        # controller.fillPolygon = not controller.fillPolygon


    elif key == glfw.KEY_ESCAPE:
        sys.exit()

    elif action == glfw.PRESS and key == glfw.KEY_O:
        # controller.follow_player = not controller.follow_player
        controller.showFps = not controller.showFps
    elif action == glfw.PRESS and key == glfw.KEY_M:
        controller.debug = not controller.debug
    elif action == glfw.PRESS and mods == glfw.MOD_ALT and key == glfw.KEY_ENTER:
        if not controller.fullScreen:
            glfw.set_window_monitor(window, glfw.get_primary_monitor(), 0, 0, controller.width, controller.height, 60)
        else:
            glfw.set_window_monitor(window, None, 0, 0, controller.width, controller.height, 60)
        controller.fullScreen = not controller.fullScreen
    elif action == glfw.PRESS and key == glfw.KEY_N:
        controller.noclip = not controller.noclip

    if not controller.ended:
        if action == glfw.PRESS and key == glfw.KEY_SPACE:
            controller.inventory = not controller.inventory
            controller.pause = not controller.pause

    if not controller.pause and not controller.ended:
        if action == glfw.PRESS and key == glfw.KEY_W:

            if (controller.activeLevel[int(controller.x + controller.sumaX), int(
                    controller.y + controller.sumaY)] != 1 or controller.noclip == True):
                controller.x += controller.sumaX
                controller.y += controller.sumaY
            # controller.z+= (camZ / R * dt * controller.veloc)
        elif action == glfw.PRESS and key == glfw.KEY_S:
            if (controller.activeLevel[int(controller.x - controller.sumaX), int(
                    controller.y - controller.sumaY)] != 1 or controller.noclip == True):
                controller.x -= controller.sumaX
                controller.y -= controller.sumaY

        elif action == glfw.PRESS and key == glfw.KEY_A:
            if (controller.activeLevel[int(controller.x - controller.sumaY), int(
                    controller.y + controller.sumaX)] != 1 or controller.noclip == True):
                controller.x -= controller.sumaY
                controller.y += controller.sumaX

        # else:
        elif action == glfw.PRESS and key == glfw.KEY_D:
            if (controller.activeLevel[int(controller.x + controller.sumaY), int(
                    controller.y - controller.sumaX)] != 1 or controller.noclip == True):
                controller.x += controller.sumaY
                controller.y -= controller.sumaX
        if action == glfw.PRESS and key == glfw.KEY_E:
            controller.lookDirection += 1
            if controller.lookDirection > 3:
                controller.lookDirection = 0
        elif action == glfw.PRESS and key == glfw.KEY_Q:
            controller.lookDirection -= 1
            if controller.lookDirection < 0:
                controller.lookDirection = 3
        elif action == glfw.PRESS and key == glfw.KEY_LEFT_CONTROL:
            controller.lookDirection += 2
            if controller.lookDirection > 3:
                controller.lookDirection -= 4
        elif action == glfw.PRESS and key == glfw.KEY_C:
            controller.compass = not controller.compass
        if action == glfw.PRESS and key == glfw.KEY_UP:
            '''controller.level+=1
            if controller.level in controller.levels:
                changeLevel(controller.levels[controller.level])
            else:
                controller.ended = True'''
            if controller.volume < 1.0:
                controller.volume += 0.1
                pg.mixer.music.set_volume(controller.volume)

        elif action == glfw.PRESS and key == glfw.KEY_DOWN:
            '''controller.level -= 1
            if controller.level in controller.levels:
                changeLevel(controller.levels[controller.level])
            else:
                controller.ended = True'''
            if controller.volume > 0.0:
                controller.volume -= 0.1
                pg.mixer.music.set_volume(controller.volume)




        elif action == glfw.PRESS and key == glfw.KEY_LEFT:
            pass

        # else:
        elif action == glfw.PRESS and key == glfw.KEY_RIGHT:
            pass


# Create ground with textures
def createGround(texture, textureCompressed):
    if controller.compress:
        gpuGround_texture = es.toGPUShape(
            bs.createTextureQuadNormals(textureCompressed, compressed=True), gl.GL_REPEAT,
            gl.GL_NEAREST, compressed=True)
    else:
        gpuGround_texture = es.toGPUShape(
            bs.createTextureQuadNormals(texture, compressed=False), gl.GL_REPEAT,
            gl.GL_NEAREST, compressed=False)

    ground_scaled = sg.SceneGraphNode("ground_scaled", 2)
    ground_scaled.transform = tr.scale(1, 1, 1)
    ground_scaled.childs += [gpuGround_texture]

    ground_rotated = sg.SceneGraphNode("ground_rotated_x", 2)
    ground_rotated.transform = tr.rotationX(0)
    ground_rotated.childs += [ground_scaled]

    return ground_rotated


def createCeiling(texture, textureCompressed):
    if controller.compress:
        gpuGround_texture = es.toGPUShape(
            bs.createTextureQuadNormalsInverted(textureCompressed, compressed=True), gl.GL_REPEAT,
            gl.GL_NEAREST, compressed=True)
    else:
        gpuGround_texture = es.toGPUShape(
            bs.createTextureQuadNormalsInverted(texture, compressed=False), gl.GL_REPEAT,
            gl.GL_NEAREST, compressed=False)

    ground_scaled = sg.SceneGraphNode("ground_scaled", 2)
    ground_scaled.transform = tr.scale(1, 1, 1)
    ground_scaled.childs += [gpuGround_texture]

    ground_rotated = sg.SceneGraphNode("ground_rotated_x", 2)
    ground_rotated.transform = tr.rotationX(0)
    ground_rotated.childs += [ground_scaled]

    return ground_rotated


# Create walls
def createWall(texture, textureCompressed):
    if controller.compress:
        gpuWall_texture = es.toGPUShape(bs.createTextureNormalsCube(textureCompressed, compressed=True),
                                        gl.GL_REPEAT, gl.GL_NEAREST, compressed=True)
    else:
        gpuWall_texture = es.toGPUShape(bs.createTextureNormalsCube(texture, compressed=False),
                                        gl.GL_REPEAT, gl.GL_NEAREST, compressed=False)

    wall_scaled = sg.SceneGraphNode("wall_scaled", 3)
    wall_scaled.transform = tr.scale(1, 1, 1)
    wall_scaled.childs += [gpuWall_texture]

    wall_rotated = sg.SceneGraphNode("wall_rotated_x", 3)
    wall_rotated.transform = tr.rotationX(0)
    wall_rotated.childs += [wall_scaled]

    return wall_rotated


def createArrows():
    scaleV = np.array([0.8, 2, 1])
    translate = np.array([0.8, 0.6, 0])
    uniformScale = 0.2
    arrow1_texture = es.toGPUShape(bs.createTextureQuad("Textures/Arrow/arrow1.png"), gl.GL_CLAMP_TO_EDGE,
                                   gl.GL_NEAREST)
    arrow1_scaled = sg.SceneGraphNode("arrow1_scaled")
    arrow1_scaled.transform = tr.matmul(
        [tr.translate(translate[0], translate[1], translate[2]), tr.rotationY(controller.rotation),
         tr.scale(scaleV[0], scaleV[1], scaleV[2]), tr.uniformScale(uniformScale)])
    arrow1_scaled.childs += [arrow1_texture]

    aux = list(controller.arrows)
    aux.append(arrow1_scaled)
    aux = np.asarray(aux)
    controller.arrows = aux


def createExit():
    if controller.compress:
        gpuExit_texture = es.toGPUShape(
            bs.createTextureQuadNormalsSideways("Textures/ladder.png", compressed=True), gl.GL_CLAMP_TO_EDGE,
            gl.GL_NEAREST, compressed=True)
    else:
        gpuExit_texture = es.toGPUShape(
            bs.createTextureQuadNormalsSideways("Textures/ladder.png", compressed=False), gl.GL_CLAMP_TO_EDGE,
            gl.GL_NEAREST, compressed=False)

    exit_scaled = sg.SceneGraphNode("exit_scaled", 2)
    exit_scaled.transform = tr.scale(1, 1, 1.5)
    exit_scaled.childs += [gpuExit_texture]

    exit_rotated = sg.SceneGraphNode("exit_rotated_x", 2)
    exit_rotated.transform = tr.rotationX(0)
    exit_rotated.childs += [exit_scaled]

    return exit_rotated


def createTreasure():
    gpuTreasure_texture = es.toGPUShape(bs.createTextureNormalsCube("Textures/crate.png"), gl.GL_REPEAT, gl.GL_NEAREST)
    treasure_scaled = sg.SceneGraphNode("treasure_scaled")
    treasure_scaled.transform = tr.scale(1, 1, 1)
    treasure_scaled.childs += [gpuTreasure_texture]

    treasure_rotated = sg.SceneGraphNode("wall_rotated_x")
    treasure_rotated.transform = tr.rotationX(0)
    treasure_rotated.childs += [treasure_scaled]

    gpuGold_texture = es.toGPUShape(bs.createTextureQuadNormals("Textures/gold.jpg"), gl.GL_REPEAT, gl.GL_NEAREST)

    gold_scaled = sg.SceneGraphNode("gold_scaled")
    gold_scaled.transform = tr.uniformScale(0.85)
    gold_scaled.childs += [gpuGold_texture]

    gold_rotated = sg.SceneGraphNode("gold_rotated")
    gold_rotated.transform = tr.translate(0, 0, 0.505)
    gold_rotated.childs += [gold_scaled]

    treasure_complete = sg.SceneGraphNode("treasure_complete")
    treasure_complete.childs += [gold_rotated]
    treasure_complete.childs += [treasure_rotated]

    return treasure_complete


def setLight(palette):
    controller.quadratic = palette.quadratic
    controller.lineal = palette.lineal
    controller.constant = palette.constant
    controller.ka = palette.ka
    controller.kd = palette.kd
    controller.ks = palette.ks
    controller.shininess = palette.shininess


# Create Maze
def createMaze(maze, palette):
    global controller
    setLight(palette)
    grid = maze
    groundr = createGround(palette.floor, palette.floorCompressed)
    ceilingr = createCeiling(palette.ceiling, palette.ceilingCompressed)
    # treasurer = createTreasure()
    wallr = createWall(palette.wall, palette.wallCompressed)
    exitr = createExit()

    for x in range(grid.shape[0]):
        for y in range(grid.shape[1]):
            if grid[x, y] == 1:

                wall = sg.SceneGraphNode("wall")
                wall.pos = np.array([x, y, 0])
                wall.transform = tr.translate(x, y, 0)

                wall.childs += [wallr]
                aux = list(controller.walls)
                aux.append(wall)
                aux = np.asarray(aux)
                controller.walls = aux


            elif grid[x, y] == 2:
                controller.x = x
                controller.y = y
                controller.x_start = x
                controller.y_start = y
                controller.start = True

                ground = sg.SceneGraphNode("ground", 2)
                ground.pos = np.array([x, y, -0.5])
                ground.transform = tr.translate(x, y, -0.5)
                ground.childs += [groundr]
                createPlanes(ground, ground.pos, 2)

                ceiling = sg.SceneGraphNode("ceiling", 2)
                ceiling.pos = np.array([x, y, 0])
                ceiling.transform = tr.matmul([tr.translate(x, y, 0)])

                ceiling.childs += [ceilingr]

                aux = list(controller.grounds)
                aux.append(ground)
                aux = np.asarray(aux)
                controller.grounds = aux

                aux = list(controller.ceilings)
                aux.append(ceiling)
                aux = np.asarray(aux)
                controller.ceilings = aux

            elif grid[x, y] == 4:

                ground = sg.SceneGraphNode("ground", 2)
                ground.pos = np.array([x, y, -0.5])
                ground.transform = tr.translate(x, y, -0.5)

                createPlanes(ground, ground.pos, 2)
                ground.childs += [groundr]

                ceiling = sg.SceneGraphNode("ceiling", 2)
                ceiling.pos = np.array([x, y, 0])
                ceiling.transform = tr.matmul([tr.translate(x, y, 0)])

                ceiling.childs += [ceilingr]
                createPlanes(ceiling, ceiling.pos, 2)

                aux = list(controller.grounds)
                aux.append(ground)
                aux = np.asarray(aux)
                controller.grounds = aux

                aux = list(controller.ceilings)
                aux.append(ceiling)
                aux = np.asarray(aux)
                controller.ceilings = aux
            elif grid[x, y] == 5:
                controller.x = x
                controller.y = y
                controller.x_start = x
                controller.y_start = y
                controller.start = True

                ground = sg.SceneGraphNode("ground", 2)
                ground.pos = np.array([x, y, -0.5])
                ground.transform = tr.translate(x, y, -0.5)

                ground.childs += [groundr]
                createPlanes(ground, ground.pos, 2)

                ceiling = sg.SceneGraphNode("ceiling", 2)
                ceiling.pos = np.array([x, y, 0])
                ceiling.transform = tr.matmul([tr.translate(x, y, 0)])

                ceiling.childs += [ceilingr]

                aux = list(controller.grounds)
                aux.append(ground)
                aux = np.asarray(aux)
                controller.grounds = aux

                aux = list(controller.ceilings)
                aux.append(ceiling)
                aux = np.asarray(aux)
                controller.ceilings = aux
            elif grid[x, y] == 6:
                controller.x = x
                controller.y = y
                controller.x_start = x
                controller.y_start = y
                controller.start = True

                ground = sg.SceneGraphNode("ground", 2)
                ground.pos = np.array([x, y, -0.5])
                ground.transform = tr.translate(x, y, -0.5)

                ground.childs += [groundr]
                createPlanes(ground, ground.pos, 2)

                ceiling = sg.SceneGraphNode("ceiling", 2)
                ceiling.pos = np.array([x, y, 0])
                ceiling.transform = tr.matmul([tr.translate(x, y, 0)])
                ceiling.childs += [ceilingr]

                aux = list(controller.grounds)
                aux.append(ground)
                aux = np.asarray(aux)
                controller.grounds = aux

                aux = list(controller.ceilings)
                aux.append(ceiling)
                aux = np.asarray(aux)
                controller.ceilings = aux
            elif grid[x, y] == 7:
                controller.x_exit = x
                controller.y_exit = y
                controller.exit = True

                ceiling = sg.SceneGraphNode("ceiling", 2)
                ceiling.pos = np.array([x, y, 0])
                ceiling.transform = tr.matmul([tr.translate(x, y, 0)])
                ceiling.childs += [ceilingr]

                aux = list(controller.ceilings)
                aux.append(ceiling)
                aux = np.asarray(aux)
                controller.ceilings = aux

                exit = sg.SceneGraphNode("ground", 2)
                exit.pos = np.array([x, y, -1.25])
                exit.transform = tr.translate(x, y, -1.25)
                exit.childs += [exitr]
                createPlanes(exit, exit.pos, 2)

                aux = list(controller.grounds)
                aux.append(exit)
                aux = np.asarray(aux)
                controller.grounds = aux

    if not controller.start:
        raise SystemError('No start point found')
    if not controller.exit:
        raise SystemError('No exit found')
    return


def clearMaze():
    controller.walls = np.array([])
    controller.grounds = np.array([])
    controller.ceilings = np.array([])
    controller.start = False
    controller.exit = False


def reset():
    global controller
    fullscreen = controller.fullScreen
    controller = Controller()
    controller.fullScreen = fullscreen
    initialSetup()
    controller.level = 1
    changeLevel(controller.levels[controller.level])


def createLevels():
    tecno = "Audio/Music/playonloop.com/Nuts and bolts/POL-nuts-and-bolts-short.wav"
    medieval = "Audio/Music/playonloop.com/Random encounter/POL-random-encounter-short.wav"
    terror = "Audio/Music/Spooky Music Mini Pack/Track 1/Track 1 - Cubiculacetophobie.wav"
    # Level 1
    maze = cm.createMaze(roomTries=500, roomExtraSize=1, minSize=1, maxwidth=15, maxheight=15, windingPercent=0.1,
                         extraConnectorChance=50)

    ground = "Textures/groundB.png"
    groundCompressed = "Textures/Compressed/ground.DDS"

    wall = "Textures/wallB.png"
    wallCompressed = "Textures/Compressed/wall.DDS"

    ceiling = "Textures/PixelTexturePack/Textures/Rocks/DIRT.png"
    ceilingCompressed = "Textures/Compressed/ground.DDS"

    bricks = "Textures/PixelTexturePack/Textures/Bricks/CLAYBRICKS.png"
    dungeonBricks = "Textures/PixelTexturePack/Textures/Bricks/DUNGEONBRICKS.png"

    quadratic = 0.5
    lineal = 0.1
    constant = 0.1
    ka = np.array([0.7, 0.5, 0.5])
    kd = np.array([0.8, 0.6, 0.6])
    ks = np.array([1, 1, 1])
    shininess = 2

    palette1 = TexturePalette(wall=bricks, wallCompressed=wallCompressed, ceiling=dungeonBricks,
                              ceilingCompressed=ceilingCompressed, floor=dungeonBricks,
                              floorCompressed=groundCompressed, quadratic=quadratic,
                              lineal=lineal, constant=constant, ka=ka, kd=kd, ks=ks, shininess=shininess)

    # Level 2
    maze2 = cm.createMaze(roomTries=500, roomExtraSize=0, minSize=1, maxwidth=25, maxheight=25, windingPercent=0.8,
                          extraConnectorChance=50)
    wallTech = "Textures/PixelTexturePack/Textures/Tech/TECHWALLA.png"
    hexagons = "Textures/PixelTexturePack/Textures/Tech/HEXAGONS.png"
    bigsquares = "Textures/PixelTexturePack/Textures/Tech/BIGSQUARES.png"

    ka2 = np.array([0.2, 0.2, 0.2])
    kd2 = np.array([0.4, 0.4, 0.4])
    ks2 = np.array([1, 1, 1])
    pallete2 = TexturePalette(wall=wallTech, wallCompressed=wallCompressed, ceiling=bigsquares,
                              ceilingCompressed=ceilingCompressed, floor=hexagons, floorCompressed=groundCompressed,
                              quadratic=quadratic,
                              lineal=lineal, constant=constant, ka=ka2, kd=kd2, ks=ks2, shininess=shininess)

    # Level 3
    maze3 = cm.createMaze(roomTries=1000, roomExtraSize=0, minSize=2, maxwidth=25, maxheight=25, windingPercent=0.3,
                          extraConnectorChance=50)
    gooBricks = "Textures/PixelTexturePack/Textures/Bricks/GOOBRICKS.png"
    porcelain = "Textures/PixelTexturePack/Textures/Bricks/PORCELAINBRICKS.png"

    ka3 = np.array([0, 0.01, 0])
    kd3 = np.array([0.4, 0.5, 0.4])
    ks3 = np.array([1, 1, 1])
    pallete3 = TexturePalette(wall=gooBricks, wallCompressed=wallCompressed, ceiling=gooBricks,
                              ceilingCompressed=ceilingCompressed, floor=porcelain, floorCompressed=groundCompressed,
                              quadratic=0.3,
                              lineal=0.1, constant=0.02, ka=ka3, kd=kd3, ks=ks3, shininess=100)

    level1 = Level(maze, palette1, medieval)
    level2 = Level(maze2, pallete2, tecno)
    level3 = Level(maze3, pallete3, terror)

    controller.levels[1] = level1
    controller.levels[2] = level2
    controller.levels[3] = level3


def changeLevel(level):
    clearMaze()
    try:
        pg.mixer.music.load(level.song)
        pg.mixer.music.play(-1)
        pg.mixer.music.set_volume(controller.volume)
        createMaze(level.maze, level.palette)
        controller.activeLevel = level.maze
    except:
        raise Exception("ERROR: maze not build or palette not configured")


def scroll_callback(window, xoffset, yoffset):
    if (yoffset > 0):
        controller.fov -= 5
    elif (yoffset < 0):
        controller.fov += 5


def cursor_position_callback(window, xpos, ypos):
    controller.mousex = xpos
    controller.mousey = ypos
    calculateRayCast(xpos, ypos)


def checkClose(array, test, close):
    for i in array:
        if np.abs(i[0] - test[0]) < close and np.abs(i[1] - test[1]) < close:
            return True
    return False


def returnContain(arr, iobj):
    if arr is 0:
        return False
    else:
        for i in arr:
            if iobj == i:
                return True
    return False


def getCol(array, test, close):
    for i in array:
        if np.abs(i[0] - test[0]) <= close and np.abs(i[1] - test[1]) <= close:
            return i


def createLight():
    gpuWall_texture = es.toGPUShape(bs.createColorNormalsCube(1, 1, 1))
    wall_scaled = sg.SceneGraphNode("wall_scaled")
    wall_scaled.transform = tr.scale(0.2, 0.2, 0.2)
    wall_scaled.childs += [gpuWall_texture]

    wall_rotated = sg.SceneGraphNode("wall_rotated_x")
    wall_rotated.transform = tr.translate(controller.x, controller.y, 3)
    wall_rotated.childs += [wall_scaled]

    return wall_rotated


def check(test, array):
    return any(np.array_equal(x, test) for x in array)


def loadCubeMap(faces):
    skyt = gl.glGenTextures(1)
    gl.glBindTexture(gl.GL_TEXTURE_CUBE_MAP, skyt)

    for i in range(len(faces)):
        image = Image.open(faces[i])
        img_data = np.array(list(image.getdata()), np.uint8)

        if image.mode == "RGB":
            internalFormat = gl.GL_RGB
            format = gl.GL_RGB
        elif image.mode == "RGBA":
            internalFormat = gl.GL_RGBA
            format = gl.GL_RGBA
        else:
            print("Image mode not supported.")
            raise Exception()

        gl.glTexImage2D(gl.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, internalFormat, image.size[0], image.size[1], 0,
                        format,
                        gl.GL_UNSIGNED_BYTE,
                        img_data)
        # glGenerateMipmap(GL_TEXTURE_2D)

    gl.glTexParameteri(gl.GL_TEXTURE_CUBE_MAP, gl.GL_TEXTURE_MIN_FILTER, gl.GL_LINEAR)
    gl.glTexParameteri(gl.GL_TEXTURE_CUBE_MAP, gl.GL_TEXTURE_MAG_FILTER, gl.GL_LINEAR)
    gl.glTexParameteri(gl.GL_TEXTURE_CUBE_MAP, gl.GL_TEXTURE_WRAP_S, gl.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(gl.GL_TEXTURE_CUBE_MAP, gl.GL_TEXTURE_WRAP_T, gl.GL_CLAMP_TO_EDGE)
    gl.glTexParameteri(gl.GL_TEXTURE_CUBE_MAP, gl.GL_TEXTURE_WRAP_R, gl.GL_CLAMP_TO_EDGE)
    return skyt


def blockinRadius(posBlock, r, posPlayer):
    if np.sqrt((posBlock[0] - posPlayer[0]) ** 2 + (posBlock[1] - posPlayer[1]) ** 2) <= r:
        return True
    else:
        return False


def updateView():
    if controller.lookDirection == 0:
        controller.camera_theta_fixed = np.pi / 2
        controller.camera_rho_fixed = np.pi / 2
    elif controller.lookDirection == 1:
        controller.camera_theta_fixed = np.pi / 2
        controller.camera_rho_fixed = 0
    elif controller.lookDirection == 3:
        controller.camera_theta_fixed = np.pi / 2
        controller.camera_rho_fixed = np.pi
    elif controller.lookDirection == 2:
        controller.camera_theta_fixed = np.pi / 2
        controller.camera_rho_fixed = 3 * np.pi / 2


def createInventory():
    inventory_texture = es.toGPUShape(bs.createTextureQuad("Textures/inventory.png"), gl.GL_REPEAT, gl.GL_NEAREST)

    inventory_scaled = sg.SceneGraphNode("inventory_scaled")
    inventory_scaled.transform = tr.matmul([tr.scale(0.5, 1.3, 1), tr.translate(0, 0, 0)])
    inventory_scaled.childs += [inventory_texture]

    return inventory_scaled


def createText():
    face = freetype.Face("Fonts/VCR_OSD_MONO_1.001.ttf")
    face.set_pixel_sizes(0, 100)
    createCharacterDictionary(face)


def createCharacterDictionary(face):
    gl.glPixelStorei(gl.GL_UNPACK_ALIGNMENT, 1)
    for i in range(128):
        face.load_char(i, freetype.FT_LOAD_RENDER)

        texture = gl.glGenTextures(1)
        gl.glBindTexture(gl.GL_TEXTURE_2D, texture)

        gl.glTexImage2D(gl.GL_TEXTURE_2D, 0, gl.GL_RED, face.glyph.bitmap.width, face.glyph.bitmap.rows, 0, gl.GL_RED,
                        gl.GL_UNSIGNED_BYTE, face.glyph.bitmap.buffer)
        # Set texture options
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_S, gl.GL_CLAMP_TO_EDGE)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_T, gl.GL_CLAMP_TO_EDGE)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_LINEAR)
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MAG_FILTER, gl.GL_LINEAR)
        # Now store character for later use
        character = Character(texture, np.array([face.glyph.bitmap.width, face.glyph.bitmap.rows]),
                              np.array([face.glyph.bitmap_left, face.glyph.bitmap_top]), face.glyph.advance.x)
        controller.characters[chr(i)] = character


def calculateHalf(size, width):
    x = (width - size) / 2
    return x


def renderText(shader, text, x, y, scale, color, VAO, VBO, printF=False):
    originalX = x
    gl.glUseProgram(shader.shaderProgram)
    gl.glUniform3f(gl.glGetUniformLocation(shader.shaderProgram, "textColor"), color[0], color[1], color[2])
    gl.glUniformMatrix4fv(gl.glGetUniformLocation(shader.shaderProgram, "projection"), 1, gl.GL_TRUE,
                          ortoProjection)

    gl.glActiveTexture(gl.GL_TEXTURE0)
    gl.glBindVertexArray(VAO)
    size = 0
    last = 0
    # Iterate through all the characters

    for c in text:
        ch = controller.characters[c]
        xpos = x + ch.bearing[0] * scale
        size += ch.bearing[0] * scale

        ypos = y - (ch.size[1] - ch.bearing[1]) * scale

        w = ch.size[0] * scale

        h = ch.size[1] * scale

        # Update VBO for each character
        vertices = [

            xpos, ypos + h, 0.0, 0.0,
            xpos, ypos, 0.0, 1.0,
                  xpos + w, ypos, 1.0, 1.0,

            xpos, ypos + h, 0.0, 0.0,
                  xpos + w, ypos, 1.0, 1.0,
                  xpos + w, ypos + h, 1.0, 0.0
        ]
        last = xpos + w
        textVertices = np.array(vertices, dtype=np.float32)
        # Render glyph texture over quad
        gl.glBindTexture(gl.GL_TEXTURE_2D, ch.textureID)
        # Update content of VBO memory
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, VBO)
        gl.glBufferSubData(gl.GL_ARRAY_BUFFER, 0, 24 * INT_BYTES, textVertices)
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, 0)
        # Render quad
        gl.glDrawArrays(gl.GL_TRIANGLES, 0, 6)
        # Now advance cursor for next glyph
        x += (ch.advance >> 6) * scale
    size = last - originalX
    gl.glBindVertexArray(0)
    gl.glBindTexture(gl.GL_TEXTURE_2D, 0)
    if printF:
        print(size)


def createPlanes(sg, pos, dimension):
    normalZ = np.array([0, 0, 1])
    normalX = np.array([1, 0, 0])
    normalY = np.array([0, 1, 0])

    if dimension == 2:
        sg.plane.setNormalAndPoint(normalZ, pos)


def sphereInRayCasting(c, radius):
    controller.origin = np.array([controller.x, controller.y, controller.z])

    b = np.dot(controller.rayCast, (np.subtract(controller.origin, c)))
    c = np.dot((np.subtract(controller.origin, c)), (np.subtract(controller.origin, c))) - radius ** 2
    if b ** 2 - c < 0 or controller.select:
        return False

    elif b ** 2 - c >= 0:
        roots = np.roots(np.array([1, 2 * b, c]))
        return np.minimum(roots[0], [1])


def boxRayCasting(sg, dimension):
    if dimension == 2:
        controller.origin = np.array([controller.x, controller.y, controller.z])

        if np.dot(controller.rayCast, sg.plane.normal) == 0:
            return False
        t = -(np.add(np.dot(controller.origin, sg.plane.normal), sg.plane.d)) / np.dot(controller.rayCast,
                                                                                       sg.plane.normal)

        if t < 0:
            return False
        controller.selected = controller.origin + controller.rayCast * t
        return True


def calculateRayCast(posx, posy):
    x = (2.0 * xpos) / controller.width - 1.0
    z = 1.0 - (2.0 * ypos) / controller.height
    ray_clip = np.array([x, z, -1.0, 1.0])
    controller.ray_clip = ray_clip
    ray_eye = np.matmul(np.linalg.inv(projection), ray_clip)
    # ray_eye[1]=-1.0
    ray_eye[3] = 0.0
    controller.ray_eye = ray_eye
    aux = np.matmul(np.linalg.inv(normal_view), ray_eye)

    ray_wor = np.array([aux[0], aux[1], aux[2]])

    ray_wor = ray_wor / np.sqrt(ray_wor[0] ** 2 + ray_wor[1] ** 2 + ray_wor[2] ** 2)
    controller.rayCast = ray_wor


def setInitialValues():
    global t0, ti, tin, frameCount
    t0 = glfw.get_time()
    ti = glfw.get_time()
    tin = glfw.get_time()
    frameCount = 0


def initialSetup():
    createLevels()
    createArrows()
    createText()
    setInitialValues()


def selected(sg):
    pos = sg.pos
    if (pos[0] - 0.5 > controller.selected[0] or controller.selected[0] > pos[0] + 0.5) or (
            pos[1] - 0.5 > controller.selected[1] or controller.selected[1] > pos[1] + 0.5) or (
            pos[2] - 0.5 > controller.selected[2] or controller.selected[2] > pos[2] + 0.5):
        return False
    elif pos[0] - 0.5 < controller.selected[0] < pos[0] + 0.5 and pos[1] - 0.5 < controller.selected[1] < pos[
        1] + 0.5 and pos[2] - 0.5 < controller.selected[2] < pos[2] + 0.5:
        return True


if __name__ == "__main__":
    pg.init()
    # Initialize glfw
    if not glfw.init():
        sys.exit()



    glfw.window_hint(glfw.SAMPLES, 4)
    if controller.fullScreen:
        window = glfw.create_window(controller.width, controller.height, "Window name", glfw.get_primary_monitor(),
                                    None)
    else:
        window = glfw.create_window(controller.width, controller.height, "Window name", None, None)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    # Connecting the callback function 'on_key' to handle keyboard events
    glfw.set_key_callback(window, on_key)

    # Assembling the shader program (pipeline) with both shaders

    # phongPipeline = ls.SimpleTexturePhongShaderProgram()
    phongPipeline = ls.SimpleTextureGouraudShaderProgram()
    textureShader = es.SimpleTextureTransformShaderProgram()
    textShader = es.textShader()
    # textureShader = es.SimpleTextureShaderProgram()

    gl.glUseProgram(phongPipeline.shaderProgram)
    # Telling OpenGL to use our shader program

    gl.glEnable(gl.GL_DEPTH_TEST)
    gl.glDepthFunc(gl.GL_LESS)
    gl.glEnable(gl.GL_MULTISAMPLE)
    gl.glEnable(gl.GL_BLEND)

    # Setting up the clear screen color
    gl.glClearColor(0, 0, 0, 0)
    # Sound

    # Create maze
    createLevels()
    createArrows()
    changeLevel(controller.levels[controller.level])

    # Create Inventory
    inventory = createInventory()

    # Our shapes here are always fully painted
    gl.glPolygonMode(gl.GL_FRONT, gl.GL_FILL)

    ortoProjection = tr.ortho(0.0, float(controller.width), 0.0, float(controller.height), 0.0, 1.0)
    projection = tr.perspective(controller.fov, float(controller.width) / float(controller.height), 0.1, 10)

    frustum.setCamInternals(controller.fov, float(controller.width) / float(controller.height), 0.1, 10)

    model = tr.identity()

    # xpos0 = glfw.get_cursor_pos(window)[0]
    # ypos0 = glfw.get_cursor_pos(window)[1]

    glfw.set_input_mode(window, glfw.CURSOR, glfw.CURSOR_NORMAL)
    image = Image.open("Textures/pointer1.png")

    glfw.set_cursor(window, glfw.create_cursor(image, 0, 0))
    # Text setup
    createText()

    VAO = gl.glGenVertexArrays(1)
    VBO = gl.glGenBuffers(1)
    gl.glBindVertexArray(VAO)
    gl.glBindBuffer(gl.GL_ARRAY_BUFFER, VBO)
    gl.glBufferData(gl.GL_ARRAY_BUFFER, 24 * INT_BYTES, None, gl.GL_DYNAMIC_DRAW)
    gl.glEnableVertexAttribArray(0)
    gl.glVertexAttribPointer(0, 4, gl.GL_FLOAT, gl.GL_FALSE, 4 * INT_BYTES, gl.ctypes.c_void_p(0))
    gl.glBindBuffer(gl.GL_ARRAY_BUFFER, 0)
    gl.glBindVertexArray(0)

    # Initial values, used for calculating time and pos differences
    setInitialValues()
    while not glfw.window_should_close(window):
        t1 = glfw.get_time()
        dt = t1 - t0
        t0 = t1
        frameCount += 1
        nt = glfw.get_time()
        if nt - ti >= 1.0:
            controller.fps = frameCount
            frameCount = 0
            ti = nt
        # Using GLFW to check for input events
        glfw.poll_events()

        # Filling or not the shapes depending on the controller state
        if controller.fillPolygon:
            gl.glPolygonMode(gl.GL_FRONT, gl.GL_FILL)
        else:
            gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_LINE)

        # Clearing the screen in both, color and depth
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT)
        # gl.glDisable(gl.GL_BLEND)
        # Getting the time and pos difference from the previous iteration
        # Time

        # X Pos
        # xpos = glfw.get_cursor_pos(window)[0]
        # dposx = xpos - xpos0
        # xpos0 = xpos
        # Y Pos
        # ypos = glfw.get_cursor_pos(window)[1]
        # dposy = ypos - ypos0
        # ypos0 = ypos

        # if 3.14 > controller.camera_theta + (dposy * dt / 10) > 0.2:
        # controller.camera_theta += (dposy * dt / 10)
        # controller.camera_theta_sky -= (dposy * dt / 10)

        # controller.camera_rho -= (dposx * dt / 10)
        # controller.camera_rho_sky -= (dposx * dt / 10)

        updateView()

        # Setting up the view transform

        # R = controller.zoom
        # Coordenadas cilindricas
        # camX = R*np.sin(controller.camera_theta)
        # camY = R*np.cos(controller.camera_theta)
        # camZ = camera_z *-30

        # Coordenadas esfericas
        camX = np.sin(controller.camera_theta_fixed) * np.cos(controller.camera_rho_fixed)
        camY = np.sin(controller.camera_theta_fixed) * np.sin(controller.camera_rho_fixed)
        camZ = np.cos(controller.camera_theta_fixed)
        viewPos = np.array([camX, camY, camZ])
        if np.sqrt(viewPos[0] ** 2 + viewPos[1] ** 2 + viewPos[2] ** 2) != 0:
            viewPosNor = viewPos / np.sqrt(viewPos[0] ** 2 + viewPos[1] ** 2 + viewPos[2] ** 2)
        else:
            viewPosNor = viewPos

        normal_view = tr.lookAt(
            np.array([controller.x, controller.y, 0]),
            viewPosNor + np.array([controller.x, controller.y, 0]),
            np.array([0, 0, 1])
        )



        norm = np.sqrt(camX * camX + camY * camY)

        if norm != 0:
            controller.sumaX = (int(camX) / int(norm))
            controller.sumaY = (int(camY) / int(norm))
        else:
            controller.sumaX = int(camX)
            controller.sumaY = int(camY)

        glfw.set_scroll_callback(window, scroll_callback)
        glfw.set_cursor_pos_callback(window, cursor_position_callback)
        pos = glfw.get_cursor_pos(window)
        xpos = pos[0]
        ypos = pos[1]
        controller.mousex = xpos
        controller.mousey = ypos
        calculateRayCast(xpos, ypos)

        # Scene Rendering

        gl.glUseProgram(phongPipeline.shaderProgram)

        # White light in all components: ambient, diffuse and specular.
        gl.glUniform3f(gl.glGetUniformLocation(phongPipeline.shaderProgram, "La"), 1, 1, 1)
        gl.glUniform3f(gl.glGetUniformLocation(phongPipeline.shaderProgram, "Ld"), 1, 1, 1)
        gl.glUniform3f(gl.glGetUniformLocation(phongPipeline.shaderProgram, "Ls"), 1, 1, 1)

        # Object is barely visible at only ambient. Diffuse behavior is slightly red. Sparkles are white
        gl.glUniform3f(gl.glGetUniformLocation(phongPipeline.shaderProgram, "Ka"), controller.ka[0], controller.ka[1],
                       controller.ka[2])
        gl.glUniform3f(gl.glGetUniformLocation(phongPipeline.shaderProgram, "Kd"), controller.kd[0], controller.kd[1],
                       controller.kd[2])
        gl.glUniform3f(gl.glGetUniformLocation(phongPipeline.shaderProgram, "Ks"), controller.ks[0], controller.ks[1],
                       controller.ks[2])

        # TO DO: Explore different parameter combinations to understand their effect!

        gl.glUniform3f(gl.glGetUniformLocation(phongPipeline.shaderProgram, "lightPosition"), controller.x,
                       controller.y,
                       0.45)
        gl.glUniform3f(gl.glGetUniformLocation(phongPipeline.shaderProgram, "viewPosition"), controller.x,
                       controller.y,
                       0)
        gl.glUniform1ui(gl.glGetUniformLocation(phongPipeline.shaderProgram, "shininess"), controller.shininess)

        gl.glUniform1f(gl.glGetUniformLocation(phongPipeline.shaderProgram, "constantAttenuation"),
                       controller.constant)
        gl.glUniform1f(gl.glGetUniformLocation(phongPipeline.shaderProgram, "linearAttenuation"), controller.lineal)
        gl.glUniform1f(gl.glGetUniformLocation(phongPipeline.shaderProgram, "quadraticAttenuation"),
                       controller.quadratic)

        gl.glUniformMatrix4fv(gl.glGetUniformLocation(phongPipeline.shaderProgram, "projection"), 1, gl.GL_TRUE,
                              projection)
        gl.glUniformMatrix4fv(gl.glGetUniformLocation(phongPipeline.shaderProgram, "view"), 1, gl.GL_TRUE,
                              normal_view)
        frustum.setCamDef(np.array([controller.x, controller.y, 0]),
                          viewPosNor + np.array([controller.x, controller.y, 0]),
                          np.array([0, 0, 1]))

        gl.glUniformMatrix4fv(gl.glGetUniformLocation(phongPipeline.shaderProgram, "model"), 1, gl.GL_TRUE, model)

        gl.glEnable(gl.GL_DEPTH_TEST)
        gl.glEnable(gl.GL_BLEND)
        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA)
        for i in controller.walls:
            i.distance = np.sqrt((i.pos[0] - controller.x) ** 2 + (i.pos[1] - controller.y) ** 2)
        controller.walls = sorted(controller.walls, key=lambda wall: wall.distance)

        for i in controller.grounds:
            i.distance = np.sqrt((i.pos[0] - controller.x) ** 2 + (i.pos[1] - controller.y) ** 2)
        controller.grounds = sorted(controller.grounds, key=lambda ground: ground.distance)

        for i in controller.ceilings:
            i.distance = np.sqrt((i.pos[0] - controller.x) ** 2 + (i.pos[1] - controller.y) ** 2)
        controller.ceilings = sorted(controller.ceilings, key=lambda ceiling: ceiling.distance)

        for i in controller.grounds:
            # pos = i.pos
            sg.drawSceneGraphNode(i, phongPipeline, "model")
            if i.distance >= controller.distance:
                controller.select = False
                break
        for i in controller.ceilings:
            # pos = i.pos
            sg.drawSceneGraphNode(i, phongPipeline, "model")
            if i.distance >= controller.distance:
                controller.select = False
                break
        gl.glEnable(gl.GL_CULL_FACE)
        for i in controller.walls:
            # pos = i.pos
            sg.drawSceneGraphNode(i, phongPipeline, "model")
            if i.distance >= controller.distance:
                controller.select = False
                break
            # and sphereInRayCasting(pos, np.sqrt(2) / 2) == False
            '''if (frustum.sphereInFrustum(pos, np.sqrt(2) / 2) == 2 or frustum.sphereInFrustum(pos, np.sqrt(2) / 2) == 1):
                if i.dimension == 2:
                    if boxRayCasting(i, 2) == False:
                        sg.drawSceneGraphNode(i, phongPipeline, "model")
                        if i.distance >= controller.distance:
                            controller.select = False
                            break
                    else:
                        if not selected(i):
                            sg.drawSceneGraphNode(i, phongPipeline, "model")
                            if i.distance >= controller.distance:
                                controller.select = False
                                break

                elif i.dimension == 3:
                    if sphereInRayCasting(pos, np.sqrt(2) / 2) == False:
                        sg.drawSceneGraphNode(i, phongPipeline, "model")
                        if i.distance >= controller.distance:
                            controller.select = False
                            break
                    else:
                        pass
                        controller.select = True
                        '''

        # Swap the buffers

        gl.glUseProgram(textureShader.shaderProgram)

        if controller.inventory:
            sg.drawSceneGraphNode(inventory, textureShader, "transform")

        controller.rotation = (np.pi / 2) * controller.lookDirection
        if controller.lookDirection == 0 or controller.lookDirection == 2:
            scaleV = np.array([1, 2, 1])
        else:
            scaleV = np.array([1.2, 1.5, 1])

        translate = np.array([0.8, 0.6, 0])
        uniformScale = 0.2
        arrow = sg.findNode(controller.arrows[0], "arrow1_scaled")

        arrow.transform = tr.matmul(
            [tr.translate(translate[0], translate[1], translate[2]), tr.scale(scaleV[0], scaleV[1], scaleV[2]),
             tr.uniformScale(uniformScale), tr.rotationZ(controller.rotation)])
        if controller.compass:
            sg.drawSceneGraphNode(arrow, textureShader, "transform")

        # gl.glUseProgram(textShader.shaderProgram)

        gl.glDisable(gl.GL_DEPTH_TEST)
        gl.glDisable(gl.GL_CULL_FACE)

        if controller.ended:
            if t >= 60:
                entd = t / 60
                ent = int(entd)
                dec = entd - ent
                seg = 60 * dec
                renderText(textShader,
                           "CONGRATULATIONS!!!", (calculateHalf(516, 1920)/1920)*controller.width,
                           (700/1080)*controller.height, 0.00026*controller.width, np.array([1, 1, 1]), VAO, VBO, False)
                renderText(textShader,
                           "You completed the maze in " + str(ent) + " minutes and " + str(int(seg)) + " seconds",
                           (calculateHalf(1443, 1920)/1920)*controller.width,
                           (500/1080)*controller.height, 0.00026*controller.width, np.array([1, 1, 1]), VAO, VBO, False)
                renderText(textShader,
                           "Press ENTER to reset the game or press ESC to exit.", (calculateHalf(1490, 1920)/1920)*controller.width,
                           (300/1080)*controller.height, 0.00026*controller.width, np.array([1, 1, 1]), VAO, VBO, False)

            else:
                renderText(textShader,
                           "CONGRATULATIONS!!!", (calculateHalf(516, 1920)/1920)*controller.width,
                           (700/1080)*controller.height, 0.00026*controller.width, np.array([1, 1, 1]), VAO, VBO, False)
                renderText(textShader,
                           "You completed the maze in " + str(int(t)) + " seconds",
                           (calculateHalf(1059, 1920)/1920)*controller.width,
                           (500/1080)*controller.height, 0.00026*controller.width, np.array([1, 1, 1]), VAO, VBO, False)
                renderText(textShader,
                           "Press ENTER to reset the game or press ESC to exit.", (calculateHalf(1490, 1920)/1920)*controller.width,
                           (300/1080)*controller.height, 0.00026*controller.width, np.array([1, 1, 1]), VAO, VBO, False)

        if controller.showFps:
            renderText(textShader, str(int(controller.fps)) + " Fps", 10, 10, 0.5, np.array([1, 1, 1]), VAO, VBO)
        if controller.debug:
            renderText(textShader, "Exit: " + str(controller.x_exit) + " " + str(controller.y_exit), 0.729*controller.width, 0.092*controller.height, 0.00026*controller.width,
                       np.array([1, 1, 1]), VAO, VBO)
            renderText(textShader, "Pos actual: " + str(int(controller.x)) + " " + str(int(controller.y)), 0.729*controller.width,0.0092*controller.height,
                       0.00026*controller.width,
                       np.array([1, 1, 1]), VAO, VBO)

        glfw.swap_buffers(window)

    glfw.terminate()
