# cython: language_level=3
import numpy as np
import sys

np.set_printoptions(threshold=sys.maxsize)


# Inspired by http://journal.stuffwithstuff.com/2014/12/21/rooms-and-mazes/
class Controller:
    def __init__(self, roomTries, roomExtraSize, maxwidth, maxheight, windingPercent, extraConnectorChance, minSize=1):
        self.roomTries = roomTries
        self.roomExtraSize = roomExtraSize
        self.minSize = minSize
        self.maxwidth = maxwidth
        self.maxheight = maxheight
        self.windingPercent = windingPercent
        self.extraConnectorChance = extraConnectorChance

        self.grid = np.ones((self.maxwidth, self.maxheight), dtype=int)
        self.rooms = np.array([])
        self.currentRegion = 0
        self.regions = np.zeros((self.maxheight, self.maxwidth), dtype=int)
        self.rooms = np.zeros((self.maxheight, self.maxwidth))
        self.roomCentersX = np.zeros((self.maxheight, self.maxwidth), dtype=int)
        self.roomCentersY = np.zeros((self.maxheight, self.maxwidth), dtype=int)
        self.startRegion = 0
        self.wallRegions = np.zeros((self.maxheight, self.maxwidth), dtype=object)
        self.numberOfBlocksInRange = {}
        self.start = False
        self.exit = False


controller = Controller(0,0,0,0,0,0,0)


def overlaps(room):
    for i in room:

        if controller.grid[i[0]][i[1]] != 1:
            return True
    return False


def addRooms():
    for i in range(controller.roomTries):
        size = np.random.randint(controller.minSize, 3 + controller.roomExtraSize) * 2 + 1

        rectangularity = np.random.randint(0, 1 + size / 2) * 2

        width = size
        height = size

        rand = np.random.randint(0, 1)
        if rand == 1:
            width += rectangularity
        else:
            height += rectangularity

        x = np.random.randint((controller.maxwidth - width) / 2) * 2 + 1
        y = np.random.randint((controller.maxheight - height) / 2) * 2 + 1

        room = np.array([x, y])

        for i in range(width):
            for j in range(height):
                room = np.vstack((np.array([x + i, y + j]), room))

        if (not overlaps(room)):
            controller.currentRegion += 1
            for i in room:
                carve(i[0], i[1])
                controller.rooms[i[0]][i[1]] = 1
                controller.roomCentersX[i[0]][i[1]] = x + width / 2
                controller.roomCentersY[i[0]][i[1]] = y + height / 2


def canCarve(pos, dir):
    if (pos + 3 * dir)[1] > controller.maxheight or (pos + 3 * dir)[0] > controller.maxwidth or (pos + 3 * dir)[
        1] < 0 or (pos + 3 * dir)[0] < 0:
        return False
    if controller.grid[(pos + 2 * dir)[0]][(pos + 2 * dir)[1]] == 1:
        return True
    else:
        return False


def carve(x, y):
    controller.grid[x][y] = 4
    controller.regions[x][y] = controller.currentRegion


def addJunction(x, y):
    random = np.random.randint(0, 4)
    if (random == 1):
        random2 = np.random.randint(0, 3)
        if random2 == 1:
            controller.grid[x][y] = 5
        else:
            controller.grid[x][y] = 4
    else:
        controller.grid[x][y] = 6
    return


def contains(arr, obj):
    for i in arr:
        if i == obj:
            return True
    return False


def connectRegions():
    connectorRegions = {}

    for x in range(1, controller.maxwidth - 1):
        for y in range(1, controller.maxheight - 1):
            if controller.grid[x][y] != 1:
                continue
            regions = np.array([])

            region = controller.regions[x + 1][y + 0]

            if region != 0:
                if regions.size == 0:
                    regions = np.array([region])
                else:
                    regions = np.append(regions, region)
            region = controller.regions[x - 1][y + 0]

            if region != 0:
                if regions.size == 0:
                    regions = np.array([region])

                else:
                    regions = np.append(regions, region)

            region = controller.regions[x + 0][y + 1]

            if region != 0:
                if regions.size == 0:
                    regions = np.array([region])

                else:
                    regions = np.append(regions, region)

            region = controller.regions[x + 0][y - 1]

            if region != 0:
                if regions.size == 0:
                    regions = np.array([region])

                else:
                    regions = np.append(regions, region)
            regions = np.unique(regions)
            if regions.size < 2:
                continue

            connectorRegions[x, y] = regions

    connectors = list(connectorRegions)

    merged = {}
    openRegions = np.array([])
    for i in range(1, controller.currentRegion + 1):
        merged[i] = i
        openRegions = np.append(openRegions, i)
        openRegions = np.unique(openRegions)

    while (openRegions.size > 1):
        random = np.random.randint(0, len(connectors))
        connector = connectors[random]

        addJunction(connector[0], connector[1])

        regions = np.array([])

        for i in connectorRegions[connector]:
            regions = np.append(regions, merged[i])

        dest = regions[0]
        sources = regions[1:]
        for i in range(1, controller.currentRegion + 1):
            if contains(sources, merged[i]):
                merged[i] = dest
        # Eliminate sources

        for i in sources:
            mask = openRegions != sources
            openRegions = openRegions[mask]

        # No consecutive connectors

        elimin = []
        for c in connectors:
            if contains(elimin, c):
                continue
            regions = np.array([])
            for i in connectorRegions[c]:
                if contains(merged.keys(), i):
                    regions = np.append(regions, merged[i])
                # else:
                # regions = np.append(regions, None)

            regions = np.unique(regions)

            random = np.random.randint(0, controller.extraConnectorChance)

            if np.abs(connector[0] - c[0]) <= 1 and np.abs(connector[1] - c[1]) <= 1:
                elimin.append(c)



            #  If the connector no long spans different regions, we don't need it.
            elif regions.size > 1:
                pass
            # This connecter isn't needed, but connect it occasionally so that the
            # dungeon isn't singly-connected.
            else:
                elimin.append(c)
                if random == 1:
                    addJunction(c[0], c[1])

                    for d in connectors:
                        if np.abs(d[0] - c[0]) <= 1 and np.abs(d[1] - c[1]) <= 1:

                            elimin.append(d)

        elimin = list(set(elimin))
        for i in elimin:
            connectors.remove(i)


def growMaze(x, y):
    controller.currentRegion += 1
    carve(x, y)

    cells = np.array([[x, y]])
    lastDir = np.array([])

    while (cells.size != 0):

        unmadecells = np.array([])
        cell = cells[-1]

        if canCarve(cell, np.array([1, 0])):
            if unmadecells.size == 0:
                unmadecells = np.array([[1, 0]])
            else:
                unmadecells = np.vstack((np.array([[1, 0]]), unmadecells))

        if canCarve(cell, np.array([-1, 0])):
            if unmadecells.size == 0:
                unmadecells = np.array([[-1, 0]])
            else:
                unmadecells = np.vstack((np.array([[-1, 0]]), unmadecells))
        if canCarve(cell, np.array([0, 1])):
            if unmadecells.size == 0:
                unmadecells = np.array([[0, 1]])
            else:
                unmadecells = np.vstack((np.array([[0, 1]]), unmadecells))
        if canCarve(cell, np.array([0, -1])):
            if unmadecells.size == 0:
                unmadecells = np.array([[0, -1]])
            else:
                unmadecells = np.vstack((np.array([[0, -1]]), unmadecells))

        if unmadecells.size != 0:

            if lastDir in unmadecells and (np.random.random() > controller.windingPercent):
                dir = lastDir
            else:
                random = np.random.randint(0, unmadecells.size / 2)
                dir = unmadecells[random]

            carve((cell + dir)[0], (cell + dir)[1])
            carve((cell + dir * 2)[0], (cell + dir * 2)[1])
            cells = np.vstack((np.array([[(cell + dir * 2)[0], (cell + dir * 2)[1]]]), cells))
            lastDir = dir
        else:

            cells = cells[:-1]
            lastDir = np.array([])

    return


def removeDeadEnds():
    done = False
    while not done:
        done = True
        for x in range(controller.maxwidth):
            for y in range(controller.maxheight):
                if controller.grid[x][y] == 1:
                    continue
                exits = 0
                if controller.grid[x + 1][y + 0] != 1:
                    exits += 1
                if controller.grid[x - 1][y + 0] != 1:
                    exits += 1
                if controller.grid[x + 0][y + 1] != 1:
                    exits += 1
                if controller.grid[x + 0][y - 1] != 1:
                    exits += 1
                if exits != 1:
                    continue
                done = False
                controller.grid[x][y] = 1

    return


def removeExtraWalls():
    for x in range(controller.maxwidth - 1):
        for y in range(controller.maxheight - 1):
            if controller.grid[x][y] != 1:
                continue
            if ((controller.grid[x + 1][y + 0] != 1 and controller.grid[x + 1][y + 0] != 0) or (
                    controller.grid[x - 1][y + 0] != 1 and controller.grid[x - 1][y + 0] != 0) or (
                    controller.grid[x + 0][y + 1] != 1 and controller.grid[x + 0][y + 1] != 0) or (
                    controller.grid[x + 0][y - 1] != 1 and controller.grid[x + 0][y - 1] != 0)):
                continue
            else:
                controller.grid[x][y] = 0
    if ((controller.grid[controller.maxwidth - 2][controller.maxheight - 1] != 1 and
         controller.grid[controller.maxwidth - 2][controller.maxheight - 1] != 0) or (
            controller.grid[controller.maxwidth - 1][controller.maxheight - 2] != 1 and
            controller.grid[controller.maxwidth - 1][controller.maxheight - 2] != 0)):
        pass
    else:
        controller.grid[controller.maxwidth - 1][controller.maxheight - 1] = 0
    for x in range(controller.maxwidth - 1):
        if ((controller.grid[x][controller.maxheight - 2] != 1 and controller.grid[x][controller.maxheight - 2] != 0)):
            pass
        else:
            controller.grid[x][controller.maxheight - 1] = 0
    for y in range(controller.maxheight - 1):
        if ((controller.grid[controller.maxheight - 2][y] != 1 and controller.grid[controller.maxheight - 2][y] != 0)):
            pass
        else:
            controller.grid[controller.maxheight - 1][y] = 0


def putStartPos():
    # Has to be done after the level has been created
    while not controller.start:
        regions = list()
        for x in range(controller.maxwidth):
            for y in range(controller.maxheight):
                if controller.rooms[x][y] == 1:
                    regions.append(controller.regions[x][y])
        regions = np.unique(np.asarray(regions))
        # np.savetxt("regions.txt", regions.astype(int), fmt='%02d', delimiter=",")

        random = np.random.randint(0, np.size(regions))

        randomregion = regions[random]
        controller.startRegion = randomregion

        for x in range(controller.maxwidth):
            for y in range(controller.maxheight):

                if controller.grid[x][y] != 4:
                    continue

                elif controller.regions[x][y] != randomregion:
                    continue

                else:
                    centerX = controller.roomCentersX[x][y]
                    centerY = controller.roomCentersY[x][y]
                    controller.grid[int(centerX)][int(centerY)] = 2
                    return
        for x in range(controller.maxwidth):
            for y in range(controller.maxheight):
                if controller.grid[x][y] == 2:
                    controller.start = True
                    break


def putExitPos():
    # Has to be done after the level has been created
    while not controller.exit:
        regions = list()
        for x in range(controller.maxwidth):
            for y in range(controller.maxheight):
                if controller.rooms[x][y] == 1 and controller.regions[x][y] != controller.startRegion:
                    regions.append(controller.regions[x][y])
        regions = np.unique(np.asarray(regions))
        # np.savetxt("regions.txt", regions.astype(int), fmt='%02d', delimiter=",")

        random = np.random.randint(0, np.size(regions))

        randomregion = regions[random]

        for x in range(controller.maxwidth):
            for y in range(controller.maxheight):

                if controller.grid[x][y] != 4:
                    continue

                elif controller.regions[x][y] != randomregion:
                    continue

                else:
                    centerX = controller.roomCentersX[x][y]
                    centerY = controller.roomCentersY[x][y]
                    controller.grid[int(centerX)][int(centerY)] = 7
                    return
        for x in range(controller.maxwidth):
            for y in range(controller.maxheight):
                if controller.grid[x][y] == 7:
                    controller.exit = True
                    break


def putTreasures(number):
    actual = 0
    for x in range(controller.maxwidth - 1):
        for y in range(controller.maxheight - 1):
            if controller.grid[x][y] != 4:
                continue
            random = np.random.randint(0, 30)
            if random == 1:
                controller.grid[x][y] = 3
                actual += 1
                if actual == number:
                    return


def returnContain(arr, iobj):
    if arr == 0:
        return
    else:
        for i in arr:
            if iobj == i:
                return True
    return False


def wallRegions():
    for x in range(controller.maxwidth - 1):
        for y in range(controller.maxheight - 1):

            newRegions = np.array([])
            controller.wallRegions[x][y] = newRegions
            if controller.grid[x][y] != 1:
                continue

            if controller.regions[x + 1][y + 0] != 0:
                aux = list(newRegions)
                aux.append(np.array([controller.regions[x + 1][y + 0]]))
                aux = np.asarray(aux)
                newRegions = aux


            if controller.regions[x - 1][y + 0] != 0:

                aux = list(newRegions)
                aux.append(np.array([controller.regions[x - 1][y + 0]]))
                aux = np.asarray(aux)
                newRegions = aux


            if controller.regions[x + 0][y + 1] != 0:

                aux = list(newRegions)
                aux.append(np.array([controller.regions[x + 0][y + 1]]))
                aux = np.asarray(aux)
                newRegions = aux

            if controller.regions[x + 0][y - 1] != 0:

                aux = list(newRegions)
                aux.append(np.array([controller.regions[x + 0][y - 1]]))
                aux = np.asarray(aux)
                newRegions = aux
            controller.wallRegions[x][y] = newRegions
    for x in range(controller.maxwidth):
        newRegions = np.array([])
        if controller.grid[x][controller.maxheight - 1] != 1:
            continue

        if controller.regions[x][controller.maxheight - 1] != 0:

            aux = list(newRegions)
            aux.append(np.array([controller.regions[x][controller.maxheight - 2]]))
            aux = np.asarray(aux)
            newRegions = aux

    for y in range(controller.maxheight):
        newRegions = np.array([])
        if controller.grid[controller.maxwidth - 1][y] != 1:
            continue

        if controller.regions[controller.maxwidth - 2][y] != 0:

            aux = list(newRegions)
            aux.append(np.array([controller.regions[controller.maxwidth - 2][y]]))
            aux = np.asarray(aux)
            newRegions = aux


def printMaze():
    np.savetxt("maze.txt", controller.grid.astype(int), fmt='%02d', delimiter=",")

    np.save('maze.npy', controller.grid)
    txt = open("maze.txt", "r")
    for line in txt:
        for i in line:
            if i == "0":
                print("\033[0;30m \u0020", end='')
            elif i == "1":
                print("\033[0;30m \u0020", end='')
                # print("\033[0;30m .", end='')
            elif i == "2":
                print("\033[1;37;42m 2", end='')
            elif i == "3":
                print("\033[1;37;43m 3", end='')
            elif i == "4":
                print("\033[1;37;47m 4", end='')
            elif i == "5":
                print("\033[0;33;44m 5", end='')
            elif i == "6":
                print("\033[0;33;46m 6", end='')
            elif i == "7":
                print("\033[0;33;40m 7", end='')
        print("")

    np.savetxt("controller.regions.txt", controller.regions.astype(int), fmt='%02d', delimiter=",")
    np.savetxt("controller.rooms.txt", controller.rooms.astype(int), fmt='%02d', delimiter=",")


def createMaze(roomTries, roomExtraSize, minSize, maxwidth, maxheight, windingPercent, extraConnectorChance):
    global controller
    controller = Controller(roomTries=roomTries, roomExtraSize=roomExtraSize, minSize=minSize, maxwidth=maxwidth,
                            maxheight=maxheight, windingPercent=windingPercent,
                            extraConnectorChance=extraConnectorChance)

    # 0:Nothing
    # 1:Walls
    # 2:Start
    # 3:Treasure
    # 4:Ground
    # 5:Open Door
    # 6:Closed Door
    # 7:Exit

    if controller.maxwidth % 2 == 0 or controller.maxheight % 2 == 0:
        raise ValueError("The stage must be odd-sized")

    # createRooms
    addRooms()

    # Fill the empty positions with mazes
    for x in range(1, controller.maxwidth, 2):
        for y in range(1, controller.maxheight, 2):
            if controller.grid[x][y] == 1:
                growMaze(x, y)

    connectRegions()
    removeDeadEnds()
    removeExtraWalls()
    putStartPos()
    putExitPos()
    return controller.grid
