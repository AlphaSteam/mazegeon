# coding=<utf-8>
from PIL import Image
import numpy as np
from functools import partial

from pyffi.formats.dds import DdsFormat
import inspect
import struct


fo = open("Textures/wall3_PNG_DXT5_1.DDS", "rb")
filecode=struct.unpack("<4s", fo.read(4))[0]
print(filecode)
if filecode.decode("ascii") != "DDS ":
    fo.close
    raise Exception()

header = fo.read(124)

height=struct.unpack_from("<I",header[8:])[0]
width=struct.unpack_from("<I",header[12:])[0]
linearSize=struct.unpack_from("<I",header[16:])[0]
print(linearSize)
mipMapCount=struct.unpack_from("<I",header[24:])[0]
print(mipMapCount)
fourCC=struct.unpack_from("<I",header[80:])[0]
print(fourCC)

if



