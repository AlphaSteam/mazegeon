import numpy as np

class Plane:
    def __init__(self):
        self.normal = np.zeros(3)
        self.point = np.zeros(3)
        self.d = 0

    def setNormalAndPoint(self, normal, point):
        self.normal = normal
        if (np.sqrt(self.normal[0] ** 2 + self.normal[1] ** 2 + self.normal[2] ** 2)) != 0:
            self.normal = self.normal / (np.sqrt(self.normal[0] ** 2 + self.normal[1] ** 2 + self.normal[2] ** 2))

        self.d = -(self.normal[0] * point[0] + self.normal[1] * point[1] + self.normal[2] * point[2])

    def distance(self, p):
        return np.add(self.d, (self.normal[0] * p[0] + self.normal[1] * p[1] + self.normal[2] * p[2]))